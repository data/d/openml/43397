# OpenML dataset: Airlines-Tweets-Sentiments

https://www.openml.org/d/43397

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
A dataset I used to classify tweets about my company.
I took tweets and I classified them manually as positive, negative or neutral.
Content
There are 4 columns :
Id : the tweed id, unique.
tweettext : the tweet
tweetlang : always EN, all tweets are in english
tweetsentiment_value : 0 for negative, 1 for neutral, 2 for positive
Acknowledgements
Do what you want with it.
Inspiration
The aim of this dataset is to be able to determine if a tweet is positive or negative about an airline company.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43397) of an [OpenML dataset](https://www.openml.org/d/43397). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43397/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43397/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43397/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

